Small job test javascript app for Contribe:
hosted at:
http://contribejobtest.azurewebsites.net
C/P of requirements:
_____________________________________________________________________________________________________
A	model	for	Web	Developers

Objectives: To	create	a	single-page	application	(SPA)	with	the	connection	to	the	picture	

service	Flickr	to	select	and	view	images	from	a	particular	search.

User	scenario	(1)	The	user	should	be	able	to	navigate	to	a	page	with	a	search	function	(free	

text).

(2)	When	the	user	performing	a	search	will	be	photos	from	Flickr	that	match	the	search	are	

shown.

(3)	The	user	can	select	a	number	of	images	to	create	a	gallery	of	these	by	clicking	"view	

gallery".	The	user	can	also	choose	to	do	the	search.

(4)	When	the	user	chosen	photos	to	a	gallery	and	click	"show	Gallery"	presents	all	the	

images	in	small	format.

(5)	If	the	user	clicks	on	an	image,	it	is	displayed	in	a	larger	size.

Rules: No	third-party	libraries	(such	Angular	or	Jquery)	are	allowed.

Ready	solution	will	be	public	on	Github	and	Bitbucket.

To	test-drive	the	solution	will	only	need	to	clone	the	repot	of	the	above	services	and	double-
click	index.html.	Thus,	only	HTML,	CSS	and	Javascript	enabled.

To	submit	the	task	email	the	link	to	your	repo	to	arbetsprov@contribe.se